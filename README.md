# Hotel Booking System

This is an implementation exercise  to give the project team a short but useful insight into design and coding approach/style used by the aspiring candidate

## Overview
Build a set of microservices that support a simple hotel booking website that allows to perform CRUD operations on a booking by integrating with a Database and a 3rd Party Service for hotels

## Functional Requirements
- As a client, I can create a new booking
- As a client, I can read an existing booking
- As a client, I can change an existing booking
- As a client, I can delete an existing booking
- As a client, I can send a notification for bookings
> Note: This requirements are in priority order, use time boxing to deliver as many as you can in suggested time<

## Technical Requirements
- No client UI required
- APIs run as embedded containers
- Candidate can choose to persist booking against a DB or integrate with mocked 3rd party service (SOAP interface)
- Each API should take less than 500 milliseconds
- Build a mock that returns reference data for Hotels (pref SOAP)

## Data Model
- Booking
-- Number of nights
-- BookingStatus
- Customer
-- Customer Name
-- Customer Email
- Hotel
-- Hotel Name

## Technology
- Language: Java 8
- Frameworks: Spring Boot, Spring core, Gradle
- Testing: Junit 5, Mockito
- Database: MongoDB
- ThirdParty Mock: SOAPUI
- Source code repository: Git

## Review criteria
- REST URL naming and design approach to services
- usage of java features with comments & coding conventions/style
- usage of spring boot and other spring features
- testing approach for services
- considerations like performance, security, logging, configurability
> Please submit the code and artifact as response with any documentation <

## Other
- Candidate should try to complete this exercise in 4-8 hours
- Candidate can choose an IDE like IntelliJ, Eclipse etc
- Choice of code repository: GitHub, Bitbucket etc
- Candidate can use libraries, approach that can support the implementation
