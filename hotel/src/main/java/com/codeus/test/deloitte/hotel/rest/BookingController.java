package com.codeus.test.deloitte.hotel.rest;

import com.codeus.test.deloitte.hotel.exceptions.BookingNotFoundException;
import com.codeus.test.deloitte.hotel.model.Booking;
import com.codeus.test.deloitte.hotel.repo.BookingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.math.BigInteger;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
public class BookingController {
    @Autowired
    BookingRepository bookingRepository;

    @PostMapping("/bookings")
    public ResponseEntity<Object> createBooking(@RequestBody Booking booking) {
        Booking savedBooking = bookingRepository.save(booking);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(savedBooking.getId()).toUri();

        return ResponseEntity.created(location).build();

    }

    @GetMapping("/bookings")
    public List<Booking> retrieveAllBookings() {
        return bookingRepository.findAll();
    }

    @GetMapping("/bookings/{id}")
    public Booking retrieveBooking(@PathVariable BigInteger id) throws BookingNotFoundException {
        Optional<Booking> booking = bookingRepository.findById(id);

        if (!booking.isPresent()) {
            throw new BookingNotFoundException(id);
        }

        return booking.get();
    }

    @PutMapping("/bookings/{id}")
    public ResponseEntity<Object> updateBooking(@RequestBody Booking booking, @PathVariable BigInteger id) {
        Optional<Booking> bookingOptional = bookingRepository.findById(id);

        if (!bookingOptional.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        booking.setId(id);
        bookingRepository.save(booking);

        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/bookings/{id}")
    public void deleteBooking(@PathVariable BigInteger id) {
        bookingRepository.deleteById(id);
    }
}
