package com.codeus.test.deloitte.hotel.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "customers")
@Data @NoArgsConstructor @RequiredArgsConstructor
public class Customer {
    @Id
    @NonNull
    private String email;

    @NonNull
    private String name;
}
