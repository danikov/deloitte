package com.codeus.test.deloitte.hotel.model.converters;

import com.codeus.test.deloitte.hotel.model.Customer;
import com.codeus.test.deloitte.hotel.repo.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class StringToCustomerConverter implements Converter<String, Customer> {
    @Autowired
    CustomerRepository customerRepository;

    @Override
    public Customer convert(String email) {
        return customerRepository.findByEmail(email).get(0);
    }
}
