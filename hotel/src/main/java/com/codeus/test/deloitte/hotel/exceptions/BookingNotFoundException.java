package com.codeus.test.deloitte.hotel.exceptions;

import java.math.BigInteger;

public class BookingNotFoundException extends Exception {
    public BookingNotFoundException(BigInteger id) {
        super("id-" + id);
    }
}
