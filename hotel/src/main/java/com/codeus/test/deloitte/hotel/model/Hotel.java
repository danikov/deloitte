package com.codeus.test.deloitte.hotel.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

@Data @NoArgsConstructor @RequiredArgsConstructor
public class Hotel {
    @Id
    @NonNull
    private String name;
}
