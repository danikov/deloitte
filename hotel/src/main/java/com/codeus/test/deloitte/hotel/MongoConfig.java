package com.codeus.test.deloitte.hotel;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories(basePackages = "com.codeus.test.deloitte.hotel.repo")
public class MongoConfig {
}