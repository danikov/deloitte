package com.codeus.test.deloitte.hotel.repo;

import com.codeus.test.deloitte.hotel.model.Booking;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository
public interface BookingRepository extends MongoRepository<Booking, BigInteger> {
}
