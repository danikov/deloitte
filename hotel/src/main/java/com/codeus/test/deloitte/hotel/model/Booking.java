package com.codeus.test.deloitte.hotel.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.ReadOnlyProperty;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigInteger;

@Document(collection = "bookings")
@Data @NoArgsConstructor
public class Booking {
    @Id
    @ReadOnlyProperty
    private BigInteger id;

    @NonNull
    private Hotel hotel;

    @NonNull
    private Customer customer;

    @NonNull
    private Integer nights;

    @NonNull
    @ReadOnlyProperty
    private BookingStatus status;
}
