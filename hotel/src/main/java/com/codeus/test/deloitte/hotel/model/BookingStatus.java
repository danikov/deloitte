package com.codeus.test.deloitte.hotel.model;

public enum BookingStatus {
    PENDING,
    ACCEPTED,
    REJECTED,
    CANCELLED
}
