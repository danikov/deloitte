package com.codeus.test.deloitte.hotel;

import com.codeus.test.deloitte.hotel.repo.BookingRepository;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class HotelApplication {

	public static void main(String[] args) {
		SpringApplication.run(HotelApplication.class, args);
	}

	@Bean
	ApplicationRunner init(BookingRepository repository) {
		return args -> {
			repository.findAll().forEach(System.out::println);
		};
	}
}
