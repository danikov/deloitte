package com.codeus.test.deloitte.hotel;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestHotelApplication.class)
public class HotelApplicationTests {

	@Test
	public void contextLoads() {
	}

}
