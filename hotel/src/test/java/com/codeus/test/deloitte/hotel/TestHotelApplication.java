package com.codeus.test.deloitte.hotel;

import com.codeus.test.deloitte.hotel.model.Customer;
import com.codeus.test.deloitte.hotel.model.Hotel;
import com.codeus.test.deloitte.hotel.repo.BookingRepository;
import com.codeus.test.deloitte.hotel.repo.CustomerRepository;
import com.codeus.test.deloitte.hotel.repo.HotelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class TestHotelApplication {

	@Autowired
	BookingRepository bookingRepository;

	@Autowired
	CustomerRepository customerRepository;

	@Autowired
	HotelRepository hotelRepository;

	public static void main(String[] args) {
		SpringApplication.run(TestHotelApplication.class, args);
	}

	@Bean
	ApplicationRunner init() {
		return args -> {

			// populate reference data
			customerRepository.insert(new Customer("tom@example.com", "Tom"));
			customerRepository.insert(new Customer("dick@example.com", "Dick"));
			customerRepository.insert(new Customer("harry@example.com", "Harry"));

			hotelRepository.insert(new Hotel("Hilton"));
			hotelRepository.insert(new Hotel("Marriott"));
			hotelRepository.insert(new Hotel("Radisson"));

			customerRepository.findAll().forEach(System.out::println);
			hotelRepository.findAll().forEach(System.out::println);
		};
	}
}
