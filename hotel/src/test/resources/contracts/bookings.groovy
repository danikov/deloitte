package contracts

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'POST'
        url '/bookings'
        headers {
            header 'Content-Type': 'application/json'
        }
        body(file('create_booking_request.json'))
    }
    response {
        status (201)
        //TODO expect body of created booking w/ fields
    }
}

//
//Contract.make {
//    request {
//        method 'GET'
//        url '/bookings'
//    }
//    response {
//        // TODO
//    }
//}
//
//Contract.make {
//    request {
//        method 'GET'
//        url '/bookings/1'
//    }
//    response {
//        // TODO
//    }
//}
//
//Contract.make {
//    request {
//        method 'PUT'
//        url '/bookings/1'
//    }
//    response {
//        // TODO
//    }
//}
//
//Contract.make {
//    request {
//        method 'DELETE'
//        url '/api/bookings/1'
//    }
//    response {
//        // TODO
//    }
//}